/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.overriding;

/**
 *
 * @author ASUS
 */
public class Son extends Mother {
      
    //If there is no toString in Son then toString can be used
    //because tiths or extends are from Mother.
    @Override
    public String toString() {
        return attribute + " -> Extends -> " + super.toString();
    }
    
    
}
